package handson.two.employee.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.two.employee.models.Employee;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}