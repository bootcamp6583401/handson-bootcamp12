package handson.two.employee.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.two.employee.models.Employee;
import handson.two.employee.repositories.EmployeeRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/employees")
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewEmployee(@RequestParam String name, @RequestParam String email) {

        Employee n = new Employee();
        n.setName(name);
        n.setEmail(email);
        employeRepo.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Employee> getAllEmployees() {
        return employeRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable int id) {
        Optional<Employee> optionalEmployee = employeRepo.findById(id);
        if (optionalEmployee.isPresent()) {
            Employee foundEmployee = optionalEmployee.get();
            employeRepo.delete(foundEmployee);
            return new ResponseEntity<>(foundEmployee, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {
        Optional<Employee> userData = employeRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee user) {
        Optional<Employee> tutorialData = employeRepo.findById(id);

        if (tutorialData.isPresent()) {
            Employee _user = tutorialData.get();
            _user.setName(user.getName());
            _user.setEmail(user.getEmail());
            return new ResponseEntity<>(employeRepo.save(_user), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}