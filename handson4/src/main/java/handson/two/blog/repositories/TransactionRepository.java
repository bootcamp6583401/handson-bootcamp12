package handson.two.blog.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.two.blog.models.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

}