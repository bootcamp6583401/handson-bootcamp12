package handson.two.blog.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import handson.two.blog.models.Blog;

public interface BlogRepository extends CrudRepository<Blog, Integer> {
    List<Blog> findByUserId(int user_id);

}