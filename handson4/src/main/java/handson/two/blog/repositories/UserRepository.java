package handson.two.blog.repositories;

import org.springframework.data.repository.CrudRepository;

import handson.two.blog.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}