package handson.two.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.two.blog.models.Transaction;
import handson.two.blog.repositories.TransactionRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/transactions")
public class TransactionController {
    @Autowired
    private TransactionRepository transRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewTransaction(@RequestBody Transaction transaction) {

        Transaction _transaction = new Transaction();
        _transaction.setCustomerName(transaction.getCustomerName());
        _transaction.setProductName(transaction.getProductName());
        _transaction.setProductPrice(transaction.getProductPrice());
        _transaction.setQty(transaction.getQty());
        transRepo.save(_transaction);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Transaction> getAllTransactions() {
        return transRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Transaction> deleteTransaction(@PathVariable int id) {
        Optional<Transaction> optionalTransaction = transRepo.findById(id);
        if (optionalTransaction.isPresent()) {
            Transaction foundTransaction = optionalTransaction.get();
            transRepo.delete(foundTransaction);
            return new ResponseEntity<>(foundTransaction, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Transaction> getTransactionById(@PathVariable("id") int id) {
        Optional<Transaction> transData = transRepo.findById(id);

        if (transData.isPresent()) {
            return new ResponseEntity<>(transData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Transaction> updateTransaction(@PathVariable("id") int id,
            @RequestBody Transaction transaction) {
        Optional<Transaction> transData = transRepo.findById(id);

        if (transData.isPresent()) {
            Transaction _transaction = transData.get();
            _transaction.setCustomerName(transaction.getCustomerName());
            _transaction.setProductName(transaction.getProductName());
            _transaction.setProductPrice(transaction.getProductPrice());
            _transaction.setQty(transaction.getQty());
            return new ResponseEntity<>(transRepo.save(_transaction), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}