package handson.two.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import handson.two.blog.models.Blog;
import handson.two.blog.models.User;
import handson.two.blog.repositories.BlogRepository;
import handson.two.blog.repositories.UserRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/blogs")
public class BlogController {
    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private UserRepository userRepo;

    @PostMapping(path = "/")
    public @ResponseBody String addNewBlog(@RequestBody Blog blog) {

        Blog n = new Blog();
        n.setTitle(blog.getTitle());
        n.setCategory(blog.getCategory());
        n.setContent(blog.getContent());
        blogRepository.save(n);
        return "Saved";
    }

    @GetMapping(path = "/")
    public @ResponseBody Iterable<Blog> getAllBlogs() {
        return blogRepository.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Blog> deleteBlog(@PathVariable int id) {
        Optional<Blog> optionalBlog = blogRepository.findById(id);
        if (optionalBlog.isPresent()) {
            Blog foundBlog = optionalBlog.get();
            blogRepository.delete(foundBlog);
            return new ResponseEntity<>(foundBlog, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Blog> getBlogById(@PathVariable("id") int id) {
        Optional<Blog> userData = blogRepository.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Blog> updateBlog(@PathVariable("id") int id, @RequestBody Blog blog) {
        Optional<Blog> tutorialData = blogRepository.findById(id);

        if (tutorialData.isPresent()) {
            Blog _blog = tutorialData.get();
            _blog.setTitle(blog.getTitle());
            _blog.setCategory(blog.getCategory());
            _blog.setContent(blog.getContent());
            return new ResponseEntity<>(blogRepository.save(_blog), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/author/{authorId}")
    public ResponseEntity<Blog> setAuthor(@PathVariable("id") int id, @PathVariable("authorId") int authorId) {
        Optional<Blog> tutorialData = blogRepository.findById(id);
        Optional<User> authorData = userRepo.findById(authorId);

        if (tutorialData.isPresent() && authorData.isPresent()) {
            Blog _blog = tutorialData.get();
            User _author = authorData.get();

            _blog.setUser(_author);

            return new ResponseEntity<>(blogRepository.save(_blog), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}